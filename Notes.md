# Notes

## Contexte

Enregistrer la mise à jour d'un RIB.
On veut extraire de ficoba les infos de `RibBean`.

Dans le `PUT`, on envoie juste un IBAN.

Choix : un appel un plus dans le back.

```mermaid
sequenceDiagram
    autonumber
    angular ->> app: IBAN
    app ->> Ficoba: valid?
    Ficoba ->> app: OK <br/> FicobaResponse
    app ->> angular: OK <br/> RibBean
    angular ->> app: PATCH <br/> RibBean
    app ->> Esb: GET
    Esb ->> app: FicheContact (dont RibBean)
    app ->> Esb: PUT <br/> FicheContact

```

```mermaid
sequenceDiagram
    autonumber

    participant u as user
    participant b as angular (front)
    participant c as cipav (back)
    participant f as ficoba
    participant e as API ESB
    participant m as memberDigital API
    u -->> b: iban : clic sur "ajouter iban"
    b -->> c: GET /insured_person/{id}/verifyIban/{iban}
    c -->> f: ibanEntreeService.verifyIban()<br/>POST /dge/cipav/ficoba/verificationcbtr
    c -->> m: memberDigitalService.getMemberInformation(userId)
    m -->> c: Member informations
    f -->> c: FicobaResponse
    b -->> c: PATCH /{id}/fiche-contact
    c -->> e: contactDetailService.changeIban()<br/> PUT /services/rest/adherents/v1/{userId}/fiche_contact
    e -->> c: 200 OK
    c -->> b: 200 OK
    b -->> u: Mon Iban actuel est ...
```

## Pourquoi MapStruct?

En front, on veut retourner un objet .

## Mapper à la main ça veut dire quoi?

Pour la collègue, c’est plus simple de mapper à la main.

```java

import net.sibille.moncompte.bean.ficoba.FicobaResponse;

class MonMapper {

    public RibBean toRibBean(FicobaResponse ficobaResponse) {
        RibBean result = new RibBean();
        result.setXxxx(ficobaResponse.getYyyy());
        return result;
    }

}


```