package net.sibille.moncompte.bean.ficoba;

public class Caracteristique {
    private int codeCaracteristique;
    private String libelleCaracteristique;

    public Caracteristique(int codeCaracteristique, String libelleCaracteristique) {
        this.codeCaracteristique = codeCaracteristique;
        this.libelleCaracteristique = libelleCaracteristique;
    }

    public Caracteristique() {
    }

    public int getCodeCaracteristique() {
        return codeCaracteristique;
    }

    public void setCodeCaracteristique(int codeCaracteristique) {
        this.codeCaracteristique = codeCaracteristique;
    }

    public String getLibelleCaracteristique() {
        return libelleCaracteristique;
    }

    public void setLibelleCaracteristique(String libelleCaracteristique) {
        this.libelleCaracteristique = libelleCaracteristique;
    }

}
