package net.sibille.moncompte.bean.ficoba;

public class Nature {
    private Integer codeNature;
    private String libelleNature;

    public Nature() {
    }

    public Nature(Integer codeNature, String libelleNature) {
        this.codeNature = codeNature;
        this.libelleNature = libelleNature;
    }

    public Integer getCodeNature() {
        return codeNature;
    }

    public void setCodeNature(Integer codeNature) {
        this.codeNature = codeNature;
    }

    public String getLibelleNature() {
        return libelleNature;
    }

    public void setLibelleNature(String libelleNature) {
        this.libelleNature = libelleNature;
    }

    @Override
    public String toString() {
        return "Nature{" +
               "codeNature=" + codeNature +
               ", libelleNature='" + libelleNature + '\'' +
               '}';
    }
}
