package net.sibille.moncompte.bean.ficoba;

public class FicobaResponse {

    protected int numeroDemande;
    protected Response reponse;

    public FicobaResponse() {
    }

    public FicobaResponse(int numeroDemande, Response reponse) {
        this.numeroDemande = numeroDemande;
        this.reponse = reponse;
    }

    public int getNumeroDemande() {
        return numeroDemande;
    }

    public void setNumeroDemande(int numeroDemande) {
        this.numeroDemande = numeroDemande;
    }

    public Response getReponse() {
        return reponse;
    }

    public void setReponse(Response response) {
        this.reponse = response;
    }

    @Override
    public String toString() {
        return "FicobaResponse{" +
               "numeroDemande=" + numeroDemande +
               ", reponse=" + reponse +
               '}';
    }
}
