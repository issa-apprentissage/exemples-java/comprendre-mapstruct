package net.sibille.moncompte.bean.ficoba;

public class Succession {
    private Integer codeSuccession;
    private String libelleSuccession;

    public Succession() {
    }

    public Succession(Integer codeSuccession, String libelleSuccession) {
        this.codeSuccession = codeSuccession;
        this.libelleSuccession = libelleSuccession;
    }

    public Integer getCodeSuccession() {
        return codeSuccession;
    }

    public void setCodeSuccession(Integer codeSuccession) {
        this.codeSuccession = codeSuccession;
    }

    public String getLibelleSuccession() {
        return libelleSuccession;
    }

    public void setLibelleSuccession(String libelleSuccession) {
        this.libelleSuccession = libelleSuccession;
    }

    @Override
    public String toString() {
        return "Succession{" +
               "codeSuccession=" + codeSuccession +
               ", libelleSuccession='" + libelleSuccession + '\'' +
               '}';
    }
}
