package net.sibille.moncompte.bean.ficoba;

public class Complements {

    private int droitsSurCompte;
    private String typeRepertoire;
    private String numeroProfessionnel;
    private String datePeremption;

    public Complements(int droitsSurCompte, String typeRepertoire, String numeroProfessionnel, String datePeremption) {
        this.droitsSurCompte = droitsSurCompte;
        this.typeRepertoire = typeRepertoire;
        this.numeroProfessionnel = numeroProfessionnel;
        this.datePeremption = datePeremption;
    }

    public Complements() {
    }

    public int getDroitsSurCompte() {
        return droitsSurCompte;
    }

    public void setDroitsSurCompte(int droitsSurCompte) {
        this.droitsSurCompte = droitsSurCompte;
    }

    public String getTypeRepertoire() {
        return typeRepertoire;
    }

    public void setTypeRepertoire(String typeRepertoire) {
        this.typeRepertoire = typeRepertoire;
    }

    public String getNumeroProfessionnel() {
        return numeroProfessionnel;
    }

    public void setNumeroProfessionnel(String numeroProfessionnel) {
        this.numeroProfessionnel = numeroProfessionnel;
    }

    public String getDatePeremption() {
        return datePeremption;
    }

    public void setDatePeremption(String datePeremption) {
        this.datePeremption = datePeremption;
    }
}
