package net.sibille.moncompte.bean.ficoba;

public class Devise {

    private String codeDevise;
    private String libelleDevise;

    public Devise(String codeDevise, String libelleDevise) {
        this.codeDevise = codeDevise;
        this.libelleDevise = libelleDevise;
    }

    public Devise() {
    }

    public String getCodeDevise() {
        return codeDevise;
    }

    public void setCodeDevise(String codeDevise) {
        this.codeDevise = codeDevise;
    }

    public String getLibelleDevise() {
        return libelleDevise;
    }

    public void setLibelleDevise(String libelleDevise) {
        this.libelleDevise = libelleDevise;
    }
}
