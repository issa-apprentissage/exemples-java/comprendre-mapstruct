package net.sibille.moncompte.bean.ficoba;

public class PersonnePhysique {
    private String nomPatronymique;
    private String prenom;
    private String dateNaissance;
    private String sexe;
    private String nomUsage;
    private String prenomUsage;
    private String alias;
    private String codeTerritorialiteNaissance;
    private String codeDepartementNaissance;
    private String departementNaissance;
    private String codeCommuneNaissance;
    private String communeNaissance;

    public PersonnePhysique(String nomPatronymique, String prenom, String dateNaissance, String sexe, String nomUsage, String prenomUsage, String alias, String codeTerritorialiteNaissance, String codeDepartementNaissance, String departementNaissance, String codeCommuneNaissance, String communeNaissance) {
        this.nomPatronymique = nomPatronymique;
        this.prenom = prenom;
        this.dateNaissance = dateNaissance;
        this.sexe = sexe;
        this.nomUsage = nomUsage;
        this.prenomUsage = prenomUsage;
        this.alias = alias;
        this.codeTerritorialiteNaissance = codeTerritorialiteNaissance;
        this.codeDepartementNaissance = codeDepartementNaissance;
        this.departementNaissance = departementNaissance;
        this.codeCommuneNaissance = codeCommuneNaissance;
        this.communeNaissance = communeNaissance;
    }

    public PersonnePhysique() {

    }

    public String getNomPatronymique() {
        return nomPatronymique;
    }

    public void setNomPatronymique(String nomPatronymique) {
        this.nomPatronymique = nomPatronymique;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(String dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public String getNomUsage() {
        return nomUsage;
    }

    public void setNomUsage(String nomUsage) {
        this.nomUsage = nomUsage;
    }

    public String getPrenomUsage() {
        return prenomUsage;
    }

    public void setPrenomUsage(String prenomUsage) {
        this.prenomUsage = prenomUsage;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getCodeTerritorialiteNaissance() {
        return codeTerritorialiteNaissance;
    }

    public void setCodeTerritorialiteNaissance(String codeTerritorialiteNaissance) {
        this.codeTerritorialiteNaissance = codeTerritorialiteNaissance;
    }

    public String getCodeDepartementNaissance() {
        return codeDepartementNaissance;
    }

    public void setCodeDepartementNaissance(String codeDepartementNaissance) {
        this.codeDepartementNaissance = codeDepartementNaissance;
    }

    public String getDepartementNaissance() {
        return departementNaissance;
    }

    public void setDepartementNaissance(String departementNaissance) {
        this.departementNaissance = departementNaissance;
    }

    public String getCodeCommuneNaissance() {
        return codeCommuneNaissance;
    }

    public void setCodeCommuneNaissance(String codeCommuneNaissance) {
        this.codeCommuneNaissance = codeCommuneNaissance;
    }

    public String getCommuneNaissance() {
        return communeNaissance;
    }

    public void setCommuneNaissance(String communeNaissance) {
        this.communeNaissance = communeNaissance;
    }

    @Override
    public String toString() {
        return "PersonnePhysique{" +
               "nomPatronymique='" + nomPatronymique + '\'' +
               ", prenom='" + prenom + '\'' +
               ", dateNaissance='" + dateNaissance + '\'' +
               ", sexe='" + sexe + '\'' +
               ", nomUsage='" + nomUsage + '\'' +
               ", prenomUsage='" + prenomUsage + '\'' +
               ", alias='" + alias + '\'' +
               ", codeTerritorialiteNaissance='" + codeTerritorialiteNaissance + '\'' +
               ", codeDepartementNaissance='" + codeDepartementNaissance + '\'' +
               ", departementNaissance='" + departementNaissance + '\'' +
               ", codeCommuneNaissance='" + codeCommuneNaissance + '\'' +
               ", communeNaissance='" + communeNaissance + '\'' +
               '}';
    }
}
