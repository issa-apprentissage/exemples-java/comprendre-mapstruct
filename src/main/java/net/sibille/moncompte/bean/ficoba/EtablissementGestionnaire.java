package net.sibille.moncompte.bean.ficoba;

public class EtablissementGestionnaire {

    private Etablissement etablissement;
    private Guichet guichet;
    private AdressePostale adressePostale;

    public EtablissementGestionnaire(Etablissement etablissement, Guichet guichet, AdressePostale adressePostale) {
        this.etablissement = etablissement;
        this.guichet = guichet;
        this.adressePostale = adressePostale;
    }

    public EtablissementGestionnaire() {
    }


    public Etablissement getEtablissement() {
        return etablissement;
    }

    public void setEtablissement(Etablissement etablissement) {
        this.etablissement = etablissement;
    }

    public Guichet getGuichet() {
        return guichet;
    }

    public void setGuichet(Guichet guichet) {
        this.guichet = guichet;
    }

    public AdressePostale getAdressePostale() {
        return adressePostale;
    }

    public void setAdressePostale(AdressePostale adressePostale) {
        this.adressePostale = adressePostale;
    }

    @Override
    public String toString() {
        return "EtablissementGestionnaire{" +
               "etablissement=" + etablissement +
               ", guichet=" + guichet +
               ", adressePostale=" + adressePostale +
               '}';
    }
}
