package net.sibille.moncompte.bean.ficoba;

public class AdressePostale {

    private String libelle;
    private String compl;
    private String cpVille;
    private String lieuDit;

    public AdressePostale(String libelle, String compl, String cpVille, String lieuDit) {
        this.libelle = libelle;
        this.compl = compl;
        this.cpVille = cpVille;
        this.lieuDit = lieuDit;
    }

    public AdressePostale() {
    }


    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getCompl() {
        return compl;
    }

    public void setCompl(String compl) {
        this.compl = compl;
    }

    public String getCpVille() {
        return cpVille;
    }

    public void setCpVille(String cpVille) {
        this.cpVille = cpVille;
    }

    public String getLieuDit() {
        return lieuDit;
    }

    public void setLieuDit(String lieuDit) {
        this.lieuDit = lieuDit;
    }
}
