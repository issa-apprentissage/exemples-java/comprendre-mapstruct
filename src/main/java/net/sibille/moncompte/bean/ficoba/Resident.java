package net.sibille.moncompte.bean.ficoba;

public class Resident {
    private Integer codeResident;
    private String libelleResident;


    public Resident(Integer codeResident, String libelleResident) {
        this.codeResident = codeResident;
        this.libelleResident = libelleResident;
    }

    public Resident() {
    }

    public Integer getCodeResident() {
        return codeResident;
    }

    public void setCodeResident(Integer codeResident) {
        this.codeResident = codeResident;
    }

    public String getLibelleResident() {
        return libelleResident;
    }

    public void setLibelleResident(String libelleResident) {
        this.libelleResident = libelleResident;
    }

    @Override
    public String toString() {
        return "Resident{" +
               "codeResident=" + codeResident +
               ", libelleResident='" + libelleResident + '\'' +
               '}';
    }
}
