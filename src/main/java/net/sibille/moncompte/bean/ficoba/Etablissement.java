package net.sibille.moncompte.bean.ficoba;

public class Etablissement {

    private int numeroEtablissement;
    private String libelleEtablissement;

    public Etablissement(int numeroEtablissement, String libelleEtablissement) {
        this.numeroEtablissement = numeroEtablissement;
        this.libelleEtablissement = libelleEtablissement;
    }

    public Etablissement() {
    }

    public int getNumeroEtablissement() {
        return numeroEtablissement;
    }

    public void setNumeroEtablissement(int numeroEtablissement) {
        this.numeroEtablissement = numeroEtablissement;
    }

    public String getLibelleEtablissement() {
        return libelleEtablissement;
    }

    public void setLibelleEtablissement(String libelleEtablissement) {
        this.libelleEtablissement = libelleEtablissement;
    }
}
