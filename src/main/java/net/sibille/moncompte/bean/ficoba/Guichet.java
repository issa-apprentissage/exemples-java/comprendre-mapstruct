package net.sibille.moncompte.bean.ficoba;

public class Guichet {

    private int numeroGuichet;
    private String libelleGuichet;

    public Guichet(int numeroGuichet, String libelleGuichet) {
        this.numeroGuichet = numeroGuichet;
        this.libelleGuichet = libelleGuichet;
    }

    public Guichet() {
    }

    public int getNumeroGuichet() {
        return numeroGuichet;
    }

    public void setNumeroGuichet(int numeroGuichet) {
        this.numeroGuichet = numeroGuichet;
    }

    public String getLibelleGuichet() {
        return libelleGuichet;
    }

    public void setLibelleGuichet(String libelleGuichet) {
        this.libelleGuichet = libelleGuichet;
    }

    @Override
    public String toString() {
        return "Guichet{" +
               "numeroGuichet=" + numeroGuichet +
               ", libelleGuichet='" + libelleGuichet + '\'' +
               '}';
    }
}
