package net.sibille.moncompte.bean.ficoba;

import java.io.Serializable;

/**
 * FicheContactBean
 */
public class FicheContactBean implements Serializable {

    private String mobile;

    private String email;

    private AddressBean address;

    // le Rib de l'utilisateur s'il existe
    private RibBean rib;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public AddressBean getAddress() {
        return address;
    }

    public void setAddress(AddressBean address) {
        this.address = address;
    }

    public RibBean getRib() {
        return rib;
    }

    public void setRib(RibBean rib) {
        this.rib = rib;
    }

    public static class EmailBean {

        private String email;

        public String getEmail() {
            return this.email;
        }

        public void setEmail(String email) {
            this.email = email;
        }
    }

    public static class EmailChangeBean {

        private Long num_adherent;

        private String email;

        public Long getNum_adherent() {
            return this.num_adherent;
        }


        public void setNum_dherent(Long numAdherent) {
            this.num_adherent = numAdherent;
        }

        public String getEmail() {
            return this.email;
        }

        public void setEmail(String email) {
            this.email = email;
        }
    }

    public static class MobileBean {

        private String mobile;

        public String getMobile() {
            return this.mobile;
        }

        public void setMobile(String phone) {
            this.mobile = phone;
        }
    }

    public static class AddressBean {

        private String house_number;

        private String street;

        private String detail1;

        private String detail2;

        private String detail3;

        private String zipcode;

        private String city;

        private String country_code;

        public String getHouse_number() {
            return house_number;
        }

        public void setHouse_number(String house_number) {
            this.house_number = house_number;
        }

        public String getStreet() {
            return street;
        }

        public void setStreet(String street) {
            this.street = street;
        }

        public String getDetail1() {
            return detail1;
        }

        public void setDetail1(String detail1) {
            this.detail1 = detail1;
        }

        public String getDetail2() {
            return detail2;
        }

        public void setDetail2(String detail2) {
            this.detail2 = detail2;
        }

        public String getDetail3() {
            return detail3;
        }

        public void setDetail3(String detail3) {
            this.detail3 = detail3;
        }

        public String getZipcode() {
            return zipcode;
        }

        public void setZipcode(String zipCode) {
            this.zipcode = zipCode;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getCountry_code() {
            return country_code;
        }

        public void setCountry_code(String country_code) {
            if (country_code.length() > 2) { // hack pour retourner le code iso du pays à partir de la Locale
                this.country_code = country_code.substring(3);
            } else {
                this.country_code = country_code;
            }
        }
    }

    public static class RibBean {

        private String codeCivilite;
        private String libelleCivilite;
        private String nomTitulaire;
        private String prenomTitulaire;
        private boolean compteJoint;
        private String iban;
        private String bic;
        private String codeFormat;
        private String libelleFormat;

        public String getCodeFormat() {
            return codeFormat;
        }

        public void setCodeFormat(String codeFormat) {
            this.codeFormat = codeFormat;
        }

        public String getLibelleFormat() {
            return libelleFormat;
        }

        public void setLibelleFormat(String libelleFormat) {
            this.libelleFormat = libelleFormat;
        }

        public String getCodeCivilite() {
            return codeCivilite;
        }

        public void setCodeCivilite(String codeCivilite) {
            this.codeCivilite = codeCivilite;
        }

        public String getLibelleCivilite() {
            return libelleCivilite;
        }

        public void setLibelleCivilite(String libelleCivilite) {
            this.libelleCivilite = libelleCivilite;
        }

        public String getNomTitulaire() {
            return nomTitulaire;
        }

        public void setNomTitulaire(String nomTitulaire) {
            this.nomTitulaire = nomTitulaire;
        }

        public String getPrenomTitulaire() {
            return prenomTitulaire;
        }

        public void setPrenomTitulaire(String prenomTitulaire) {
            this.prenomTitulaire = prenomTitulaire;
        }

        public boolean isCompteJoint() {
            return compteJoint;
        }

        public void setCompteJoint(boolean compteJoint) {
            this.compteJoint = compteJoint;
        }

        public String getIban() {
            return iban;
        }

        public void setIban(String iban) {
            this.iban = iban;
        }

        public String getBic() {
            return bic;
        }

        public void setBic(String bic) {
            this.bic = bic;
        }

        public String render() {
            return "RibBean{\n" +
                   "  codeCivilite='" + codeCivilite + '\'' + "\n" +
                   ", libelleCivilite='" + libelleCivilite + '\'' + "\n" +
                   ", nomTitulaire='" + nomTitulaire + '\'' + "\n" +
                   ", prenomTitulaire='" + prenomTitulaire + '\'' + "\n" +
                   ", compteJoint=" + compteJoint + "\n" +
                   ", iban='" + iban + '\'' + "\n" +
                   ", bic='" + bic + '\'' + "\n" +
                   ", codeFormat='" + codeFormat + '\'' + "\n" +
                   ", libelleFormat='" + libelleFormat + '\'' + "\n" +
                   '}';
        }

    }

}