package net.sibille.moncompte.bean.ficoba;

import java.util.List;

public class Response {

    protected int nombreComptes;
    protected List<Compte> comptes;

    public Response(int nombreComptes, List<Compte> comptes) {
        this.nombreComptes = nombreComptes;
        this.comptes = comptes;
    }

    public Response() {
    }

    public int getNombreComptes() {
        return nombreComptes;
    }

    public void setNombreComptes(int nombreComptes) {
        this.nombreComptes = nombreComptes;
    }

    public List<Compte> getComptes() {
        return comptes;
    }

    public void setComptes(List<Compte> comptes) {
        this.comptes = comptes;
    }

    @Override
    public String toString() {
        return "Response{" +
               "nombreComptes=" + nombreComptes +
               ", comptes=" + comptes +
               '}';
    }
}
