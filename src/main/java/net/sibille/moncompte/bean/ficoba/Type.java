package net.sibille.moncompte.bean.ficoba;

public class Type {
    private Integer codeType;
    private String libelleType;


    public Type(Integer codeType, String libelleType) {
        this.codeType = codeType;
        this.libelleType = libelleType;
    }

    public Type() {
    }

    public Integer getCodeType() {
        return codeType;
    }

    public void setCodeType(Integer codeType) {
        this.codeType = codeType;
    }

    public String getLibelleType() {
        return libelleType;
    }

    public void setLibelleType(String libelleType) {
        this.libelleType = libelleType;
    }

    @Override
    public String toString() {
        return "Type{" +
               "codeType=" + codeType +
               ", libelleType='" + libelleType + '\'' +
               '}';
    }
}
