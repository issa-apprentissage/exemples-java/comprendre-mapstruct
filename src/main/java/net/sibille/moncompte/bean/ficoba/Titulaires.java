package net.sibille.moncompte.bean.ficoba;

public class Titulaires {

    private PersonnePhysique personnePhysique;
    private Complements complements;
    private Adresse adresse;

    public Titulaires(PersonnePhysique personnePhysique, Complements complements, Adresse adresse) {
        this.personnePhysique = personnePhysique;
        this.complements = complements;
        this.adresse = adresse;
    }

    public Titulaires() {
    }

    public PersonnePhysique getPersonnePhysique() {
        return personnePhysique;
    }

    public void setPersonnePhysique(PersonnePhysique personnePhysique) {
        this.personnePhysique = personnePhysique;
    }

    public Complements getComplements() {
        return complements;
    }

    public void setComplements(Complements complements) {
        this.complements = complements;
    }

    public Adresse getAdresse() {
        return adresse;
    }

    public void setAdresse(Adresse adresse) {
        this.adresse = adresse;
    }

    @Override
    public String toString() {
        return "Titulaires{" +
               "personnePhysique=" + personnePhysique +
               ", adresse=" + adresse +
               '}';
    }
}
