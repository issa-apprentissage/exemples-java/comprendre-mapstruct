package net.sibille.moncompte.bean.ficoba;

import java.util.List;

public class Compte {
    private List<CaracteristiquesCompte> caracteristiquesCompte;
    private EtablissementGestionnaire etablissementGestionnaire;
    private List<Titulaires> titulaires;

    public Compte(List<CaracteristiquesCompte> caracteristiquesCompte, EtablissementGestionnaire etablissementGestionnaire, List<Titulaires> titulaires) {
        this.caracteristiquesCompte = caracteristiquesCompte;
        this.etablissementGestionnaire = etablissementGestionnaire;
        this.titulaires = titulaires;
    }

    public Compte() {

    }

    public List<CaracteristiquesCompte> getCaracteristiquesCompte() {
        return caracteristiquesCompte;
    }

    public void setCaracteristiquesCompte(List<CaracteristiquesCompte> caracteristiquesCompte) {
        this.caracteristiquesCompte = caracteristiquesCompte;
    }

    public EtablissementGestionnaire getEtablissementGestionnaire() {
        return etablissementGestionnaire;
    }

    public void setEtablissementGestionnaire(EtablissementGestionnaire etablissementGestionnaire) {
        this.etablissementGestionnaire = etablissementGestionnaire;
    }

    public List<Titulaires> getTitulaires() {
        return titulaires;
    }

    public void setTitulaires(List<Titulaires> titulaires) {
        this.titulaires = titulaires;
    }

    @Override
    public String toString() {
        return "Compte{" +
               "caracteristiquesCompte=" + caracteristiquesCompte +
               ", titulaires=" + titulaires +
               '}';
    }
}
