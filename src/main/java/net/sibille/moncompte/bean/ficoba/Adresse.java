package net.sibille.moncompte.bean.ficoba;

public class Adresse {
    private Integer codeTerritorialiteResidence;
    private String codeDepartementResidence;
    private String departementResidence;
    private String codeCommuneResidence;
    private String communeResidence;
    private String zoneVoie;
    private String lieuDit;
    private String datePeremption;

    public Adresse(Integer codeTerritorialiteResidence, String codeDepartementResidence, String departementResidence, String codeCommuneResidence, String communeResidence, String zoneVoie, String lieuDit, String datePeremption) {
        this.codeTerritorialiteResidence = codeTerritorialiteResidence;
        this.codeDepartementResidence = codeDepartementResidence;
        this.departementResidence = departementResidence;
        this.codeCommuneResidence = codeCommuneResidence;
        this.communeResidence = communeResidence;
        this.zoneVoie = zoneVoie;
        this.lieuDit = lieuDit;
        this.datePeremption = datePeremption;
    }

    public Adresse() {

    }

    public Integer getCodeTerritorialiteResidence() {
        return codeTerritorialiteResidence;
    }

    public void setCodeTerritorialiteResidence(Integer codeTerritorialiteResidence) {
        this.codeTerritorialiteResidence = codeTerritorialiteResidence;
    }

    public String getCodeDepartementResidence() {
        return codeDepartementResidence;
    }

    public void setCodeDepartementResidence(String codeDepartementResidence) {
        this.codeDepartementResidence = codeDepartementResidence;
    }

    public String getDepartementResidence() {
        return departementResidence;
    }

    public void setDepartementResidence(String departementResidence) {
        this.departementResidence = departementResidence;
    }

    public String getCodeCommuneResidence() {
        return codeCommuneResidence;
    }

    public void setCodeCommuneResidence(String codeCommuneResidence) {
        this.codeCommuneResidence = codeCommuneResidence;
    }

    public String getCommuneResidence() {
        return communeResidence;
    }

    public void setCommuneResidence(String communeResidence) {
        this.communeResidence = communeResidence;
    }

    public String getZoneVoie() {
        return zoneVoie;
    }

    public void setZoneVoie(String zoneVoie) {
        this.zoneVoie = zoneVoie;
    }

    public String getLieuDit() {
        return lieuDit;
    }

    public void setLieuDit(String lieuDit) {
        this.lieuDit = lieuDit;
    }

    public String getDatePeremption() {
        return datePeremption;
    }

    public void setDatePeremption(String datePeremption) {
        this.datePeremption = datePeremption;
    }

    @Override
    public String toString() {
        return "Adresse{" +
               "codeTerritorialiteResidence=" + codeTerritorialiteResidence +
               ", codeDepartementResidence='" + codeDepartementResidence + '\'' +
               ", departementResidence='" + departementResidence + '\'' +
               ", codeCommuneResidence='" + codeCommuneResidence + '\'' +
               ", communeResidence='" + communeResidence + '\'' +
               ", zoneVoie='" + zoneVoie + '\'' +
               ", lieuDit='" + lieuDit + '\'' +
               ", datePeremption='" + datePeremption + '\'' +
               '}';
    }
}
