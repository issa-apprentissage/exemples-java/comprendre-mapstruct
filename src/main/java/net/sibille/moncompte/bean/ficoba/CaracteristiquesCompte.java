package net.sibille.moncompte.bean.ficoba;

public class CaracteristiquesCompte {
    private String bic;
    private String iban;
    private String dateOuverture;
    private String dateCloture;
    // La nature dtermine s'il s'agit d'un compte courant, d'un compte cheque postal ou encore d'un compte de paiement
    private Nature nature;
    private Caracteristique caracteristique;
    private Succession succession;
    private Resident resident;
    private Devise devise;
    // Le type dtermine si le compte est joint ou non
    private Type type;
    private String dateHistoriqueCompte;


    public CaracteristiquesCompte(String bic, String iban, String dateOuverture, String dateCloture) {
        this.bic = bic;
        this.iban = iban;
        this.dateOuverture = dateOuverture;
        this.dateCloture = dateCloture;
    }

    public CaracteristiquesCompte() {

    }

    public String getBic() {
        return bic;
    }

    public void setBic(String bic) {
        this.bic = bic;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getDateOuverture() {
        return dateOuverture;
    }

    public void setDateOuverture(String dateOuverture) {
        this.dateOuverture = dateOuverture;
    }

    public String getDateCloture() {
        return dateCloture;
    }

    public void setDateCloture(String dateCloture) {
        this.dateCloture = dateCloture;
    }

    public Succession getSuccession() {
        return succession;
    }

    public void setSuccession(Succession succession) {
        this.succession = succession;
    }

    public Resident getResident() {
        return resident;
    }

    public void setResident(Resident resident) {
        this.resident = resident;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Nature getNature() {
        return nature;
    }

    public void setNature(Nature nature) {
        this.nature = nature;
    }

    public Caracteristique getCaracteristique() {
        return caracteristique;
    }

    public void setCaracteristique(Caracteristique caracteristique) {
        this.caracteristique = caracteristique;
    }

    public Devise getDevise() {
        return devise;
    }

    public void setDevise(Devise devise) {
        this.devise = devise;
    }

    public String getDateHistoriqueCompte() {
        return dateHistoriqueCompte;
    }

    public void setDateHistoriqueCompte(String dateHistoriqueCompte) {
        this.dateHistoriqueCompte = dateHistoriqueCompte;
    }
}

