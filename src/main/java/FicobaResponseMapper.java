import net.sibille.moncompte.bean.ficoba.CaracteristiquesCompte;
import net.sibille.moncompte.bean.ficoba.Compte;
import net.sibille.moncompte.bean.ficoba.FicheContactBean;
import net.sibille.moncompte.bean.ficoba.FicobaResponse;
import net.sibille.moncompte.bean.ficoba.Response;

import java.util.List;

public class FicobaResponseMapper {

    static FicheContactBean.RibBean toRibBean(FicobaResponse ficobaResponse) {
        if (ficobaResponse == null) return null;
        if (ficobaResponse.getReponse() == null) return new FicheContactBean.RibBean();

        FicheContactBean.RibBean ribBean = new FicheContactBean.RibBean();

        //   Ici, tu implémentes la logique de ton mapper
        //   Par example pour l'Iban :
        ribBean.setIban(ibanFrom(ficobaResponse));
        // TODO: 3️⃣
        //   mapper bic
//           ribBean.setBic(bicFrom(ficobaResponse));
        return ribBean;
    }

    private static String ibanFrom(FicobaResponse ficobaResponse) {
        var caracteristiquesCompte = firstCaracteristiqueCompte(ficobaResponse);
        if (caracteristiquesCompte == null) return null;
        return caracteristiquesCompte.getIban();
    }
//TODO: 3️⃣ exemple de mapping pour le champ Bic
//    private static String bicFrom(FicobaResponse ficobaResponse) {
//        var caracteristiquesCompte = firstCaracteristiqueCompte(ficobaResponse);
//        if (caracteristiquesCompte == null) return null;
//        return caracteristiquesCompte.getBic();
//    }

    private static CaracteristiquesCompte firstCaracteristiqueCompte(FicobaResponse ficobaResponse) {
        Response reponse = ficobaResponse.getReponse();
        List<Compte> comptes = reponse.getComptes();
        if (comptes != null && !comptes.isEmpty()) {
            var firstCompte = comptes.get(0);
            List<CaracteristiquesCompte> caracteristiquesCompte = firstCompte.getCaracteristiquesCompte();
            if (caracteristiquesCompte != null && !caracteristiquesCompte.isEmpty()) {
                return caracteristiquesCompte.get(0);
            }
        }
        return null;
    }
}