import net.sibille.moncompte.bean.ficoba.CaracteristiquesCompte;
import net.sibille.moncompte.bean.ficoba.Compte;
import net.sibille.moncompte.bean.ficoba.EtablissementGestionnaire;
import net.sibille.moncompte.bean.ficoba.FicheContactBean;
import net.sibille.moncompte.bean.ficoba.FicobaResponse;
import net.sibille.moncompte.bean.ficoba.Response;
import net.sibille.moncompte.bean.ficoba.Titulaires;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class FicobaResponseToRibBeanMapperTest {

    @Test
    void renderTest() {
        assertThat(new FicheContactBean.RibBean().render()).isEqualTo(new FicheContactBean.RibBean().render());
    }

    @Test
    void titulaire_d_un_seul_compte_non_joint_Test() {
        String codeCivilite = "codeCivilite";
        String libelleCivilite = "libelleCivilite";
        String nomTitulaire = "nomTitulaire";
        String prenomTitulaire = "prenomTitulaire";
        boolean compteJoint = false;
        String iban = "iban";
        String bic = "bic";
        String codeFormat = "codeFormat";
        String libelleFormat = "libelleFormat";

        // Given
        FicobaResponse ficobaResponse = createFicobaResponse(
                codeCivilite,
                libelleCivilite,
                nomTitulaire,
                prenomTitulaire,
                compteJoint,
                iban,
                bic,
                codeFormat,
                libelleFormat);

        // When
        FicheContactBean.RibBean ribBean = mapToRibBean(ficobaResponse);


        // Then
        FicheContactBean.RibBean expected = new FicheContactBean.RibBean();
        expected.setIban(iban);
        //TODO: 1️⃣ décommenter pour tester la prise en compte du BIC dans le mapping
//          expected.setBic(bic);

        //TODO: 2️⃣
        //  S'assurer que le bic est présent à l'appel de createFicobaResponse(...)
        //  J'ai laissé un TODO pour te guider

        //TODO: 3️⃣
        //  Pour passer le test, il faudra implémenter le comportement dans la classe FicobaResponseMapper
        //  J'y ai également laissé un TODO pour te guider

        // TODO: pour chaque champ, répéter les étapes 1️⃣, 2️⃣, 3️⃣
        // expected.setCodeCivilite(codeCivilite);
        // expected.setLibelleCivilite(libelleCivilite);
        // expected.setNomTitulaire(nomTitulaire);
        // expected.setPrenomTitulaire(prenomTitulaire);
        // expected.setCompteJoint(compteJoint);
        // expected.setCodeFormat(codeFormat);
        // expected.setLibelleFormat(libelleFormat);

        assertThat(ribBean.render()).isEqualTo(expected.render());
    }

    private FicheContactBean.RibBean mapToRibBean(FicobaResponse ficobaResponse) {
        return FicobaResponseMapper.toRibBean(ficobaResponse);
    }

    // Ici, une methode helper pour construire un FicobaResponse avec juste le minimum nécessaire pour notre test
    // Ca permet aussi d'améliorer la lisibilité du test
    private static FicobaResponse createFicobaResponse(
            String codeCivilite,
            String libelleCivilite,
            String nomTitulaire,
            String prenomTitulaire,
            boolean compteJoint,
            String iban,
            String bic,
            String codeFormat,
            String libelleFormat) {

        // TODO: Ici, à chaque fois que tu veux tester un mapping de FicobaResponse -> RibBean,
        //   tu ajoutes le champ là d'où il vient dans ce FicobaResponse

        List<Compte> comptes = new ArrayList<>();
        comptes.add(createCompte(iban  /*,bic*/));
        Response response = new Response(1, comptes);

        FicobaResponse ficobaResponse = new FicobaResponse();
        ficobaResponse.setReponse(response);

        return ficobaResponse;
    }

    private static Compte createCompte(String iban  /*, String bic*/) {
        List<CaracteristiquesCompte> caracteristiquesCompteList = new ArrayList<>();
        caracteristiquesCompteList.add(createCaracteristiquesCompte(iban  /*,bic*/));

        Titulaires titulaires = new Titulaires();
        List<Titulaires> titulaires1 = new ArrayList<>();
        titulaires1.add(titulaires);
        Compte compte = new Compte(
                caracteristiquesCompteList,
                new EtablissementGestionnaire(),
                titulaires1
        );
        return compte;
    }

    private static CaracteristiquesCompte createCaracteristiquesCompte(String iban  /*, String bic*/) {
        CaracteristiquesCompte caracteristiquesCompte = new CaracteristiquesCompte();

        // Ici, on a ajouté la valeur de l'iban à la première caractéristique du premier compte
        caracteristiquesCompte.setIban(iban);
        // TODO: 2️⃣ décommenter pour que le BIC soit ajouté à la grappe
//           caracteristiquesCompte.setBic(bic);
        return caracteristiquesCompte;
    }

}
